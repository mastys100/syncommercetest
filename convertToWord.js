
function convertToWord (value) {
    var dec = ['','one ','two ','three ','four ', 'five ','six ','seven ','eight ','nine ','ten ','eleven ','twelve ','thirteen ','fourteen ','fifteen ','sixteen ','seventeen ','eighteen ','nineteen '];
    var double = ['', '', 'twenty','thirty','forty','fifty', 'sixty','seventy','eighty','ninety'];
    //Check if the values digits are more than 8 places
    if ((value = value.toString()).length > 9)
        return 'Sorry, I can\'t handle this';
    //Attach the value to highest processible digits
    th = ('000000000' + value).substr(-9).match(/^(\d{2})(\d{2})(\d{2})(\d{1})(\d{2})$/);
   // console.log(th);
    if (!th) return; var str = '';
    str += (th[1] != 0) ? (dec[Number(th[1])] || double[th[1][0]] + ' ' + dec[th[1][1]]) + 'billion ' : '';
    str += (th[2] != 0) ? (dec[Number(th[2])] || double[th[2][0]] + ' ' + dec[th[2][1]]) + 'million ' : '';
    str += (th[3] != 0) ? (dec[Number(th[3])] || double[th[3][0]] + ' ' + dec[th[3][1]]) + 'thousand ' : '';
    str += (th[4] != 0) ? (dec[Number(th[4])] || double[th[4][0]] + ' ' + dec[th[4][1]]) + 'hundred ' : '';
    str += (th[5] != 0) ? ((str != '') ? 'and ' : '') + (dec[Number(th[5])] || double[th[5][0]] + ' ' + dec[th[5][1]]) : '';
    return str;
}