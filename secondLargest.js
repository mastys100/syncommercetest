var secondLargest = function (array){
    //check if its an array
    if(!Array.isArray(array)){
        console.log("Invalid array");
        return;
    }
    var secondLargest,max,index;
    // Find highest value in the array
    max = Math.max.apply(null, array);
    //Lets get the index of the highest
    index = array.indexOf(max);
    //We then take out this value from the array list
        array.splice(index, 1);
     secondLargest = Math.max.apply(null, array);
    return secondLargest
};


secondLargest(5);